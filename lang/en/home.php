<?php

return [

    'home' => 'Home',
    'services' => 'Services',
    'gallery' => 'Gallery',
    'features' => 'Features',
    'award' => 'Advertising ',
    'pricing' => 'Pricing',
    'faq' => 'Faq',
    'Our_services' => 'Our Services',
    'Our_gallery' => 'Our Gallery',
    'Amazing_features' => 'Amazing Features',
    'Frequently' => 'Frequently Asked Questions',
    'Follw_us' => 'Follw us:',
    'Phone_number' => 'Phone Number :',
    'Fast_links' => 'Fast Links',
    'email'     => 'Email'

];
