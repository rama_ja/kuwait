<?php

return [


    'header' => ' المقدمة ',
    'appearance' => 'إعدادات الظهور',
    'settings' => 'الإعدادات ',
    'services' => 'خدماتنا',
    'gallery' => 'معرض الصور',
    'features' => 'المميزات',
    'award' => 'الإعلانات',
    'frequently_questions' => 'FAQ',
    'social_media' => 'التواصل الاجتماعي',
    'prices' => 'الحزم',
    'prices_text' => 'نص الحزم',
    'logout'=>'تسجيل الخروج',
    'call number'=>'رقم الهاتف',
    'whatsapp'=>'whatsapp',
    'admins'=>' مديري النظام ',
    'users'=>' المستخدمون العادييون  ',
    'all_users'=>' المستخدمون  ',
    'sections'=>' أسماء الأقسام  ',
    'general_settings'=>' الإعدادت العامة   ',
    'content'=>' المحتويات ',
    'comunication'=>' التواصل ',
    'mail'=>'البريد الالكتروني  ',



];
