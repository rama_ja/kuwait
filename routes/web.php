<?php

use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\CertificationController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\FeatureController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\HeaderController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\MetaController;
use App\Http\Controllers\Admin\MissionController;
use App\Http\Controllers\Admin\NoteController;
use App\Http\Controllers\Admin\PriceController;
use App\Http\Controllers\Admin\PriceTextController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ShortNewsController;
use App\Http\Controllers\Admin\SliderController;


use App\Http\Controllers\Admin\SocialController;
use App\Http\Controllers\Admin\SpecialtyController;
use App\Http\Controllers\Admin\SubSpecialtyController;
use App\Http\Controllers\Admin\UniversityController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\NewsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [HomeController::class, 'home']);

//Route::get('lang/{lang}', [HomeController::class, 'switchLang'])->name('lang.switch');
//Route::get('admin_lang/{lang}', [AuthController::class, 'switchLang'])->name('admin_lang.switch');
Route::get('/admin_login', [AuthController::class, 'loginView'])->name('admin_login');
Route::post('/login', [AuthController::class, 'login']);
Route::group(['prefix' => 'admin_panel','as' => 'admin_panel.'], function () {

    Route::group(['middleware' => 'auth'], function () {
        ## About us Routes
        Route::resource('about', AboutController::class);

        ## ُEmployee Routes
        Route::resource('employees', EmployeeController::class);
        Route::get('/del_employee/{id}', [EmployeeController::class, 'destroy']);

        ## News Routes
        Route::resource('news', NewsController::class);
        Route::get('/del_news/{id}', [NewsController::class, 'destroy']);

        ## Short News Routes
        Route::resource('short_news', ShortNewsController::class);
        Route::get('/del_short_news/{id}', [ShortNewsController::class, 'destroy']);

        ## Missions Routes
        Route::resource('missions', MissionController::class);
        Route::get('/del_mission/{id}', [MissionController::class, 'destroy']);

        ## Missions Routes
        Route::resource('certifications', CertificationController::class);
        Route::get('/del_certification/{id}', [CertificationController::class, 'destroy']);

        ## questions Routes
        Route::resource('questions', QuestionController::class);
        Route::get('/del_question/{id}', [QuestionController::class, 'destroy']);

        ## Countries Routes
        Route::resource('countries', CountryController::class);
        Route::get('/del_country/{id}', [CountryController::class, 'destroy']);

        ## Specialty Routes
        Route::resource('specialties', SpecialtyController::class);
        Route::get('/del_specialty/{id}', [SpecialtyController::class, 'destroy']);

        ## Sub Specialty Routes
        Route::resource('sub_specialties', SubSpecialtyController::class);
        Route::get('/del_sub_specialty/{id}', [SubSpecialtyController::class, 'destroy']);

        ## Notes Routes
        Route::resource('notes', NoteController::class);
        Route::get('/del_note/{id}', [NoteController::class, 'destroy']);

        ## University Routes
        Route::resource('universities', UniversityController::class);
        Route::get('/del_university/{id}', [UniversityController::class, 'destroy']);


        ## University Notes Routes
        Route::resource('university_notes', NoteController::class);
        Route::get('/del_university_note/{id}', [NoteController::class, 'destroy']);



///////////////////////////////////////////////////////////////////////////////////////////
        ## headers Routes
        Route::resource('headers', HeaderController::class);
        Route::get('/download_header', [HeaderController::class, 'download']);

        ## gallery Routes
        Route::resource('services', ServicesController::class);
        Route::get('/del_service/{id}', [ServicesController::class, 'destroy']);

        ## gallery Routes
        Route::resource('gallery', GalleryController::class);
        Route::get('/del_gallery/{id}', [GalleryController::class, 'destroy']);

        ## features Routes
        Route::resource('features', FeatureController::class);
        Route::get('/del_feature/{id}', [FeatureController::class, 'destroy']);

        ## sliders Routes
        Route::resource('sliders', SliderController::class);
        Route::get('/del_slider/{id}', [SliderController::class, 'destroy']);
        Route::get('/download_slider/{id}', [SliderController::class, 'download']);


//        Route::resource('media', MediaController::class);

        ## price Routes
        Route::resource('prices', PriceController::class);
        Route::get('/del_price/{id}', [PriceController::class, 'destroy']);
        Route::resource('price_text', PriceTextController::class);

        ## whatsApp Routes
        Route::post('/update_whatsapp', [MediaController::class, 'updateWhatsapp']);
        Route::get('/edit_whatsapp', [MediaController::class, 'editWhatsapp']);

        ## call flyer Routes
        Route::post('/update_call', [MediaController::class, 'updateCall']);
        Route::get('/edit_call', [MediaController::class, 'editCall']);

        ## mail Routes
        Route::post('/update_mail', [MediaController::class, 'updateMail']);
        Route::get('/edit_mail', [MediaController::class, 'editMail']);

        ## phone Routes
        Route::post('/update_phone', [MediaController::class, 'updatePhone']);
        Route::get('/edit_phone', [MediaController::class, 'editPhone']);

        ## Social Routes
        Route::resource('social', SocialController::class);
        Route::get('/del_social/{id}', [SocialController::class, 'destroy']);

        ## Admins Routes
        Route::get('/users', [AdminController::class, 'users']);
        Route::get('/create_user', [AdminController::class, 'create_user']);

        Route::get('/logout', [AuthController::class, 'logout']);

    });
     Route::group(['middleware' => 'admin'], function () {
        Route::resource('settings', SettingController::class);
        Route::resource('metas', MetaController::class);

        ## sections name Routes
        Route::resource('sections', SectionController::class);
        Route::post('/update/{id}', [SettingController::class,'update']);


        ## Admins Routes
        Route::resource('admins', AdminController::class);
        Route::get('/admin_del/{id}', [AdminController::class, 'destroy']);

    });
});
//Route::get('/not_admin', [HomeController::class, 'not_admin']);
//Route::get('/{lang}', [HomeController::class, 'index']);

