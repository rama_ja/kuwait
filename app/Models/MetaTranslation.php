<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = ['title','description','og_title','keywords','footer','web_name'];

}
