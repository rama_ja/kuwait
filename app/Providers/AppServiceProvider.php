<?php

namespace App\Providers;

use App\Models\Meta;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        view()->composer('*',function($view) {
            $meta=Meta::first();
            $logo=$meta->header_logo;
            $view->with('admin', auth()->user());
            $view->with('logo', $logo);
            $view->with('meta', $meta);
        });

        Schema::defaultStringLength(191);
    }
}
