<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class SliderController extends Controller
{
    public function index(){
        $sliders=Slider::orderBy('created_at','desc')->paginate(15);
        return view('admin.sliders.index',compact('sliders'));
    }
    public function show($id){
        $slider=Slider::find($id);
        return view('admin.sliders.item',compact('slider'));
    }
    public function create(){
        return view('admin.sliders.create');
    }
    public function store(Request $request){
        if($request->file != '' ){
            $request->validate([
                'file' => 'mimes:csv,txt,xlx,xls,pdf,docx,pptx'
            ]);


            $path = 'files/sliders/';

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);

            $button_link= $filename;
            $button_type= 1;

        }elseif ($request->link != '' ){
            $button_link= $request->link;
            $button_type= 2;
        }




        if($request->image != '' ){
            $request->validate([
                'image'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);
            $path = 'images/sliders/';

            $image = $request->image;
            $imageExtension = $image->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $image->move($path, $filename);

            $image=$filename;


        }
        $slider=Slider::create([
            'button_type'    =>$button_type,
            'button_link'    =>$button_link,
            'image'    =>$image,
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar,
                'button_name' => $request->button_name_ar
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en,
                'button_name' => $request->button_name_en
            ]
        ]);

        return redirect(route('admin_panel.sliders.index'))->with('message', 'added_successfully');
    }
    public function edit(Request $request,$id){
        $slider=Slider::find($id);
        return view('admin.sliders.edit',['slider'=>$slider]);
    }

    public function update(Request $request,$id){
        $slider=Slider::find($id);
        if($request->file != '' ){
            $request->validate([
                'file' => 'mimes:csv,txt,xlx,xls,pdf,docx,pptx'
            ]);

            if($slider->button_type=='1'){
                (File::exists($slider->button_link)) ? File::delete($slider->button_link) : Null;
            }

            $path = 'files/sliders/';

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);

            $slider->update([
                'button_link'      =>$filename,
                'button_type'      =>1
            ]);

        }elseif ($request->link != '' ){
            if($slider->button_type=='1'){
                (File::exists($slider->button_link)) ? File::delete($slider->button_link) : Null;
            }

            $slider->update([
                'button_link'      =>$request->link,
                'button_type'      =>2
            ]);
        }
        if($request->image != '' ){
            $request->validate([
                'image'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);
            (File::exists($slider->image)) ? File::delete($slider->image) : Null;
            $path = 'images/sliders/';

            $image = $request->image;
            $imageExtension = $image->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $image->move($path, $filename);

            $slider->update([
                'image'      =>$filename
            ]);

        }

        $slider->update([
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar,
                'button_name' => $request->button_name_ar
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en,
                'button_name' => $request->button_name_en
            ]
        ]);

        return redirect(route('admin_panel.sliders.index'))->with('message', 'edited_successfully');
    }
    public function destroy($id){
        $slider=Slider::find($id);
        (File::exists($slider->image)) ? File::delete($slider->image) : Null;
        (File::exists($slider->button_link)) ? File::delete($slider->button_link) : Null;
        $slider->delete();
        return redirect()->back()->with('message', 'deleted_successfully');
    }
    public function download($id)
    {
        $slider=Slider::find($id);
        $file_path = $slider->button_link;
        return Response::download($file_path);
    }
}
