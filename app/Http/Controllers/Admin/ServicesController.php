<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Services;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ServicesController extends Controller
{
    public function index(){
        $services=Service::orderBy('created_at','desc')->paginate(15);
        return view ('admin.services.index',['services'=>$services]);
    }

    public function create(){
        return view('admin.services.create');
    }
    public function store(Request $request){
        if($request->file != ''){
            $path = 'images/services/';
            $request->validate([
                'file'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);



        }
        $service=Service::create([
            'image'      =>$filename,
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);


        return redirect(route('admin_panel.services.index'))->with('message', 'added_successfully');
    }
    public function edit(Request $request,$id){
        $service=Service::find($id);
        return view('admin.services.edit',['service'=>$service]);
    }

    public function update(Request $request,$id){
        $service=Service::find($id);
        if($request->file != ''){
            $request->validate([
                'file'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);
            (File::exists($service->image)) ? File::delete($service->image) : Null;
            $path = 'images/services/';

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);

            $service->update([
                'image'      =>$filename
            ]);
        }

        $service->update([
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);

        return redirect(route('admin_panel.services.index'))->with('message', 'edited_successfully');
    }
    public function destroy($id){
        $service=Service::find($id);
        (File::exists($service->image)) ? File::delete($service->image) : Null;
        $service->delete();
        return redirect()->back()->with('message', 'deleted_successfully');
    }

}
