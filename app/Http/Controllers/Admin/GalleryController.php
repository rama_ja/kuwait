<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    public function index(){
        $items=Gallery::orderBy('created_at','desc')->paginate(15);
        return view ('admin.gallery.index',['items'=>$items]);
    }

    public function create(){
        return view('admin.gallery.create');
    }
    public function store(Request $request){
        if($request->file != ''){
            $request->validate([
                'file'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);
            $path = 'images/gallery/';


            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);



        }
        $item=Gallery::create([
            'image'      =>$filename,
            'ar'=>['title'=>$request->title_ar
            ],
            'en'=>['title'=>$request->title_en
            ]
        ]);


        return redirect(route('admin_panel.gallery.index'))->with('message', 'added_successfully');
    }
    public function edit(Request $request,$id){
        $item=Gallery::find($id);
        return view('admin.gallery.edit',['item'=>$item]);
    }

    public function update(Request $request,$id){
        $item=Gallery::find($id);
        if($request->file != ''){
            $request->validate([
                'file'       => 'image|mimes:jpg,jpeg,png,gif,webp',
            ]);
            (File::exists($item->image)) ? File::delete($item->image) : Null;
            $path = 'images/gallery/';

            $file = $request->file;
            $imageExtension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $imageExtension;
            $filename = $path.$filename;
            $file->move($path, $filename);

            $item->update([
                'image'      =>$filename
            ]);
        }

        $item->update([
            'ar'=>['title'=>$request->title_ar
            ],
            'en'=>['title'=>$request->title_en
            ]
        ]);

        return redirect(route('admin_panel.gallery.index'))->with('message', 'edited_successfully');
    }
    public function destroy($id){
        $item=Gallery::find($id);
        (File::exists($item->image)) ? File::delete($item->image) : Null;
        $item->delete();
        return redirect()->back()->with('message', 'deleted_successfully');
    }

}
