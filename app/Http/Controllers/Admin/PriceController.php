<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class PriceController extends Controller
{
    public function index(){
        $prices=Price::orderBy('created_at','desc')->paginate(15);
        return view('admin.prices.index',compact('prices'));
    }
    public function show($id){
        $price=Price::find($id);
        return view('admin.prices.item',compact('price'));
    }
    public function create(){
        return view('admin.prices.create');
    }
    public function store(Request $request){

        $price=Price::create([

            'price'          =>$request->price,
            'button_link'    =>$request->link,
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar,
                'duration'    => $request->duration_ar,
                'button_name' => $request->button_name_ar
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en,
                'duration'    => $request->duration_en,
                'button_name' => $request->button_name_en
            ]
        ]);

        return redirect(route('admin_panel.prices.index'))->with('message', 'added_successfully');
    }
    public function edit(Request $request,$id){
        $price=Price::find($id);
        return view('admin.prices.edit',['price'=>$price]);
    }

    public function update(Request $request,$id){
        $price=Price::find($id);

        $price->update([
            'price'          =>$request->price,
            'button_link'    =>$request->link,
            'ar'=>['title'    => $request->title_ar,
                'description' => $request->description_ar,
                'duration'    => $request->duration_ar,
                'button_name' => $request->button_name_ar
            ],
            'en'=>['title'    => $request->title_en,
                'description' => $request->description_en,
                'duration'    => $request->duration_en,
                'button_name' => $request->button_name_en
            ]
        ]);

        return redirect(route('admin_panel.prices.index'))->with('message', 'edited_successfully');
    }
    public function destroy($id){
        $price=Price::find($id);
        $price->delete();
        return redirect()->back()->with('message', 'deleted_successfully');
    }

}
