<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FeatureController extends Controller
{
    public function index(){
        $features=Feature::orderBy('created_at','desc')->paginate(15);
        return view ('admin.features.index',['features'=>$features]);
    }

    public function create(){
        return view('admin.features.create');
    }
    public function store(Request $request){

        $feature=Feature::create([
            'image'      =>$request->file,
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);


        return redirect(route('admin_panel.features.index'))->with('message', 'added_successfully');
    }
    public function edit(Request $request,$id){
        $feature=Feature::find($id);
        return view('admin.features.edit',['feature'=>$feature]);
    }

    public function update(Request $request,$id){
        $feature=Feature::find($id);

        if($request->file!='empty'){
            $feature->update([
                'image'      =>$request->file
            ]);
        }
        $feature->update([
            'ar'=>['title'=>$request->title_ar
                ,'description' => $request->description_ar
            ],
            'en'=>['title'=>$request->title_en
                ,'description' => $request->description_en
            ]
        ]);

        return redirect(route('admin_panel.features.index'))->with('message', 'edited_successfully');
    }
    public function destroy($id){
        $feature=Feature::find($id);
        $feature->delete();
        return redirect()->back()->with('message', 'deleted_successfully');
    }
}
