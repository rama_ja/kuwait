<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use App\Models\Gallery;
use App\Models\Header;
use App\Models\Media;
use App\Models\Meta;
use App\Models\Price;
use App\Models\PriceText;
use App\Models\Question;
use App\Models\Section;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index(){
      $services=Service::get();
      $gallery=Gallery::get();
      $features=Feature::get();
      $slides=Slider::get();
      $prices=Price::get();
      $questions=Question::get();
      $media=Media::first();
      $header=Header::first();
      $meta=Meta::first();
      $price_text=PriceText::first();
      $header_app=Setting::where('name','header')->first();
      $services_app=Setting::where('name','services')->first();
      $gallery_app=Setting::where('name','gallery')->first();
      $features_app=Setting::where('name','features')->first();
      $slides_app=Setting::where('name','award')->first();
      $prices_app=Setting::where('name','prices')->first();
      $questions_app=Setting::where('name','frequently_questions')->first();
      $whatsapp_app=Setting::where('name','whatsapp')->first();
      $call_app=Setting::where('name','call number')->first();
      $home=Section::where('name','home')->first();
      $services_sec=Section::where('name','services')->first();
      $gallery_sec=Section::where('name','gallery')->first();
      $features_sec=Section::where('name','feature')->first();
      $pricing=Section::where('name','pricing')->first();
      $faq=Section::where('name','faq')->first();
      $award=Section::where('name','award')->first();
      $socials=Social::orderBy('created_at','desc')->get();
        return view('home',compact('media','header','services','gallery', 'features','slides','prices','questions','header_app', 'services_app','gallery_app','features_app','slides_app','prices_app','questions_app','whatsapp_app','call_app','price_text','meta','home','services_sec','gallery_sec','features_sec','pricing','faq','award','socials'));
    }
    public function switchLang($lang)
    {
        if (array_key_exists($lang, Config::get('languages'))) {
            Session::put('applocale', $lang);
        }
        return redirect('/'.$lang);
    }
    
     public function home(){
        $lang = App::getLocale();
        if($lang){
            return redirect('/'.$lang);
        }else{
            Session::put('applocale', 'ar');
            return redirect('/ar');
        }
    }
     public function not_admin(){
        return view('not_admin');
    }
}
