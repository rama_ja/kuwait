@php
    $lang = App::getLocale()
@endphp
    <!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="{{$meta->translate($lang)->keywords}}">
    <meta name="description" content="{{$meta->translate($lang)->description}}">

    <title>{{$meta->translate($lang)->title}}</title>
    <meta property="og:image" content="{{$meta->og_image}}">
    <meta property="og:title" content="{{$meta->translate($lang)->og_title}}">
    <meta property="og:description" content="{{$meta->translate($lang)->description}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="350" />
    <meta property="og:url" content="<?php echo URL::current(); ?>" />
    <meta property="og:site_name" content="LaunchPage">
    <meta property="og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />
    <link rel="shortcut icon" href="{{$meta->icon}}" type="image/x-icon">

    <!-- Loading Bootstrap -->
    @if($lang=='en')
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    @else
        <link href="assets/css/bootstrap-rtl.min.css" rel="stylesheet">
    @endif
    <link rel="stylesheet" href="assets/css/swiper-bundle.min.css">

    <!-- Loading Template CSS -->
    @if($lang=='en')
        <link href="assets/css/style.css" rel="stylesheet">
    @else
        <link href="assets/css/style-rtl.css" rel="stylesheet">
    @endif
    <link href="assets/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/pe-icon-7-stroke.css">
    <link href="assets/css/style-magnific-popup.css" rel="stylesheet">


    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="assets/css/all.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,700;1,400&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@600;700&display=swap" rel="stylesheet">

    <!-- Font Favicon -->
    <link rel="shortcut icon" href="{{$meta->icon}}">

</head>

<body>

<!--begin header -->
<header class="header">
    <div class="header-top">
        <div class="container">
            <div class="clearfix">
                <div class="header-info center">
                    <ul class="info-list">
                        <li>
                            <i class="fas fa-phone"></i>
                            <a target="_blank" href="tel:{{$media->phone}}">{{$media->phone}}</a>
                        </li>
                        <li>
                            <i class="fas fa-envelope"></i>
                            <a  href="#">{{$media->email}}</a>
                        </li>
                        <!-- <li>
                            <i class="fas fa-user"></i>
                            <a href="#">تسجيل أونلاين</a>
                        </li> -->
                    </ul>
                </div>
                <div class="social-links pull-left">
                    <ul class="social-list">
                        @foreach($socials as $social)
                            <li><a target="_blank" href="{{$social->link}}"><i class="{{$social->icon}}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-default navbar-fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/{{$lang}}"><img src="{{$meta->header_logo}}" width="55" height="55"></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarCollapse" style="">
                <ul class="navbar-nav ml-auto">
                    <li><a href="#home">{{$home->translate($lang)->title}}</a></li>
                    @if($services_app->status=='1')
                        <li><a href="#services">{{$services_sec->translate($lang)->title}} </a></li>
                    @endif
                    @if($gallery_app->status=='1')
                        <li><a href="#gallery">{{$gallery_sec->translate($lang)->title}} </a></li>
                    @endif
                    @if($features_app->status=='1')
                        <li><a href="#features">{{$features_sec->translate($lang)->title}} </a></li>
                    @endif
                    @if($slides_app->status=='1')
                        <li><a href="#award">{{$award->translate($lang)->title}}</a></li>
                    @endif
                    @if($prices_app->status=='1')
                        <li><a href="#pricing">{{$pricing->translate($lang)->title}} </a></li>
                    @endif
                    @if($questions_app->status=='1')
                        <li><a href="#faq">{{$faq->translate($lang)->title}}</a></li>
                    @endif

                    @if($lang=='en')
                        <li class="discover-link"><a href="{{ route('lang.switch', 'ar') }}" class="discover-btn">العربية</a></li>

                    @else
                        <li class="discover-link"><a href="{{ route('lang.switch', 'en') }}" class="discover-btn">English</a></li>
                    @endif
                </ul>

            </div>
        </div>
    </nav>
</header>

<!--end header -->

@if($header_app->status=='1')
    <!--begin home section -->
    <section class="home-section" id="home" style="background: url(../{{$header->image}}) top center no-repeat;">
        <div class="home-section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto text-center padding-top-120">
                    <h1>{{$header->translate($lang)->title}}</h1>
                    <p class="hero-text">{{$header->translate($lang)->description}}</p>
                    <div class="newsletter_form_box">
                        <p class="newsletter_success_box" style="display:none;">We received your message and you'll hear
                            from us soon. Thank You!</p>
                        <a target="_blank" href="{{$header->button_link}}" class="submit-button-newsletter">{{$header->translate($lang)->button_name}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end home section -->
@endif
@if($services_app->status=='1')
    <!--begin team section -->
    <section class="section-grey" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">{{$services_sec->translate($lang)->title}}</h2>
                </div>
                @foreach($services as $service)
                    <div class="col-sm-12 col-md-4 margin-top-30">
                        <img src="/{{$service->image}}" class="team-img width-100" alt="pic">
                        <div class="team-item">
                            <h3>{{$service->translate($lang)->title}}</h3>
                            <p>{{$service->translate($lang)->description}}</p>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
    <!--end team section-->
@endif
@if($gallery_app->status=='1')
    <!--begin section-white -->
    <section class="section-white padd-b" id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center margin-bottom-30">
                    <h2 class="section-title">{{$gallery_sec->translate($lang)->title}}</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <?php $index=1;?>
            <div class="row wow fadeIn" data-wow-delay="0.25s"
                 style="visibility: visible; animation-delay: 0.25s; animation-name: fadeIn;">
                @while($index<=$gallery->count())
                    <div class="col-md-4 col-sm-12 p-0 m-0">
                        @if($index%5==1 && $index<=$gallery->count())
                            <figure class="gallery-insta">
                                <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                                    <a class="popup2 gallery-a" href="/{{$gallery[$index-1]->image}}">
                                        <img src="/{{$gallery[$index-1]->image}}" class="width-100" alt="pic">
                                        <h3>{{$gallery[$index-1]->translate($lang)->title}}</h3>
                                        <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon"
                                                                     style="font-size: 38px;"></i></span>
                                    </a>
                                </div>
                            </figure>
                            <?php $index++;?>
                        @endif
                        @if($index%5==2 && $index<=$gallery->count())
                            <figure class="gallery-insta">
                                <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                                    <a class="popup2 gallery-a" href="/{{$gallery[$index-1]->image}}">
                                        <img src="/{{$gallery[$index-1]->image}}" class="width-100" alt="pic">
                                        <h3>{{$gallery[$index-1]->translate($lang)->title}}</h3>
                                        <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon"
                                                                     style="font-size: 38px;"></i></span>
                                    </a>
                                </div>
                            </figure>
                            <?php $index++;?>
                        @endif
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 p-0 m-0">
                        @if($index%5==3 && $index<=$gallery->count())
                            <figure class="gallery-insta">
                                <div class="popup-gallery gallery-a-1 popup-gallery-rounded portfolio-pic">
                                    <a class="popup2" href="/{{$gallery[$index-1]->image}}">
                                        <img src="/{{$gallery[$index-1]->image}}" class="width-100" alt="pic">
                                        <h3>{{$gallery[$index-1]->translate($lang)->title}}</h3>
                                        <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon"
                                                                     style="font-size: 38px;"></i></span>
                                    </a>
                                </div>
                            </figure>
                            <?php $index++;?>
                        @endif
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 p-0 m-0">
                        @if($index%5==4 && $index<=$gallery->count())
                            <figure class="gallery-insta">
                                <div class="popup-gallery  popup-gallery-rounded portfolio-pic">
                                    <a class="popup2 gallery-a" href="/{{$gallery[$index-1]->image}}">
                                        <img src="/{{$gallery[$index-1]->image}}" class="width-100" alt="pic">
                                        <h3>{{$gallery[$index-1]->translate($lang)->title}}</h3>
                                        <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon"
                                                                     style="font-size: 38px;"></i></span>
                                    </a>
                                </div>
                            </figure>
                            <?php $index++;?>
                        @endif
                        @if($index%5==0 && $index<=$gallery->count())
                            <figure class="gallery-insta">
                                <div class="popup-gallery popup-gallery-rounded portfolio-pic">
                                    <a class="popup2 gallery-a" href="/{{$gallery[$index-1]->image}}">
                                        <img src="/{{$gallery[$index-1]->image}}" class="width-100" alt="pic">
                                        <h3>{{$gallery[$index-1]->translate($lang)->title}}</h3>
                                        <span class="eye-wrapper"><i class="fa fa-search-plus eye-icon"
                                                                     style="font-size: 38px;"></i></span>
                                    </a>
                                </div>
                            </figure>
                            <?php $index++;?>
                        @endif
                    </div>
                @endwhile
            </div>
        </div>
    </section>
    <!--end section-white -->
@endif

@if($features_app->status=='1')
    <!--begin features section -->
    <section class="section-white" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center padding-bottom-10">
                    <h2 class="section-title">{{$features_sec->translate($lang)->title}}</h2>
                </div>
            </div>
            <div class="row">
                @foreach($features as $feature)
                    <div class="col-md-4">
                        <div class="feature-box light-green wow fadeIn" data-wow-delay="0.25s"
                             style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">
                            <i class="{{$feature->image}}" style="color: #e52f37"></i>
                            <div class="feature-box-text">
                                <h4>{{$feature->translate($lang)->title}}</h4>
                                <p>{{$feature->translate($lang)->description}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>

        </div>
    </section>
    <!--end features section -->
@endif
@if($slides_app->status=='1')
    <!--begin award section -->
    <section id="award">
        <div class="swiper countryswiper">
            @foreach($slides as $slide)
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="section-bg-2" style="background: url(../{{$slide->image}}) no-repeat center;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-7 mx-auto text-center">
                                        <h2 class="white-text">{{$slide->translate($lang)->title}}</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-7  mx-auto text-center">
                                        <p class="white-text">{{$slide->translate($lang)->description}}</p>
                                        <a target="_blank" href="{{$slide->button_link}}" class="btn-white small scrool">{{$slide->translate($lang)->button_name}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="swiper-button-next mai"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </section>
    <!--end award section -->
@endif
@if($prices_app->status=='1')
    <section class="section-pricing" id="pricing">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4 col-sm-12">
                    <h3>{{$price_text->translate($lang)->title}}</h3>
                    <p>{{$price_text->translate($lang)->description}}</p>
                </div>
                <div class="col-md-8">
                    <div class="swiper myswiper">
                        <div class="swiper-wrapper">

                            @foreach($prices as $price)
                                <div class="swiper-slide">
                                    <div class=" wow bounceIn" data-wow-delay="0.25s"
                                         style="visibility: visible; animation-delay: 0.25s; animation-name: bounceIn;">
                                        <div class="price-box-white">
                                            <ul class="pricing-list">
                                                <li class="price-title">{{$price->translate($lang)->title}}</li>
                                                <li class="price-value">{{$price->price}}</li>
                                                <li class="price-subtitle"> {{$price->translate($lang)->duration}}</li>
                                                <li class="price-tag"><a target="_blank" href="{{$price->button_link}}">{{$price->translate($lang)->button_name}}</a></li>
                                                <li class="price-text">{{$price->translate($lang)->description}}</li>
{{--                                                <li class="price-text">Amazing freatures.</li>--}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if($prices->count()>2)
                            <div class="swiper-button-next mai"></div>
                            <div class="swiper-button-prev"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end pricing section -->
@endif
@if($questions_app->status=='1')
    <!--begin faq section -->
    <section class="faq-page-section section-white" id="faq">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 text-center padding-bottom-10">
                    <h2 class="section-title">{{$faq->translate($lang)->title}} </h2>
                </div>
            </div>
            <div class="faq-content">
                <div class="accordion-box active-block">
                    <div class="row">
                        @foreach($questions as $question)
                            <div class="col-lg-6 col-md-12">
                                <div class="accordion block">
                                    <div class="acc-btn">
                                        <div class="icon-outer"><i class="fas fa-plus"></i></div>
                                        <h4>{{$question->translate($lang)->title}}</h4>
                                    </div>
                                    <div class="acc-content ">
                                        <div class="content">
                                            <p>{{$question->translate($lang)->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end faq section -->
@endif

<!--begin footer -->
<div class="footer">

</div>
<!--end footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="footer_one">
                    <h2><img src="{{$meta->footer_logo}}" width="55" height="55"></h2>
                    <p>{{$meta->translate($lang)->footer}}</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="footer_fast-link ">
                    <h5>{{trans('home.Fast_links')}}</h5>
                    <ul>
                        <li><a class="scrool" href="#home">{{$home->translate($lang)->title}}</a></li>
                        @if($services_app->status=='1')
                            <li><a class="scrool" href="#services">{{$services_sec->translate($lang)->title}}</a></li>
                        @endif
                        @if($gallery_app->status=='1')
                            <li><a class="scrool" href="#gallery">{{$gallery_sec->translate($lang)->title}}</a></li>
                        @endif
                        @if($features_app->status=='1')
                            <li><a class="scrool" href="#features">{{$features_sec->translate($lang)->title}} </a></li>
                        @endif
                        @if($slides_app->status=='1')
                            <li><a class="scrool" href="#award">{{$award->translate($lang)->title}} </a></li>
                        @endif
                        @if($prices_app->status=='1')
                            <li><a class="scrool" href="#pricing">{{$pricing->translate($lang)->title}} </a></li>
                        @endif
                        @if($questions_app->status=='1')
                            <li><a class="scrool" href="#faq">{{$faq->translate($lang)->title}}</a></li>
                        @endif

                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <ul class="footer_social">
                    <h5>{{trans('home.Follw_us')}}</h5>
                    @foreach($socials as $social)
                        <li><a target="_blank" href="{{$social->link}}"><i class="{{$social->icon}}"  style="color: #777"></i></a></li>
                    @endforeach
                </ul>
                <span> {{trans('home.Phone_number')}}</span>
                <a target="_blank" href="tel:{{$media->phone}}">{{$media->phone}} </a>
                <br>
                <span> {{trans('home.email')}}</span>
                <a target="_blank" href="mailto:{{$media->email}}">{{$media->email}} </a>
            </div>
        </div>
        <div class="row all-right">
            <div class="col-md-12">
                <p><span class="template-name"> </span> {{$meta->translate($lang)->footer}} <a href="{{$meta->link}}">{{$meta->translate($lang)->web_name}}</a></p>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>


@if($whatsapp_app->status=='1')
    <!--whatssapp icon -->
    <div class="icon whatsapp">
        <a target="_blank" href="https://wa.me/{{$media->whatsapp}}"><i class="fab fa-whatsapp"></i></a>

    </div>
@endif
@if($call_app->status=='1')
    <!-- phone icon -->
    <div class="icon phone">
        <a target="_blank" href="tel:{{$media->call}}"><i class="fas fa-phone"></i></a>
    </div>
@endif

<!-- Load JS here for greater good =============================-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.scrollTo-min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/jquery.nav.js"></script>
<script src="assets/js/swiper-bundle.min.js"></script>
<script src="assets/js/wow.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/custom.js"></script>
</body>

</html>
