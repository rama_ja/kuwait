@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> {{trans('sidebar.sections')}} </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="layout-spacing">

            <!-- Content -->
            <div class="col-12">
                <div class="user-profile">
                    <div class="widget-content widget-content-area">
                        <h3 class="mt-5"> </h3>

                        <form class="g-3 row" method="post" action="{{route('admin_panel.sections.update',1)}}" enctype="multipart/form-data" >
                            @method('PATCH')
                            @csrf
                            @if ($errors->any())

                                <div class="alert alert-danger">

                                    <ul style="list-style: none;margin:0">

                                        @foreach ($errors->all() as $error)

                                            <li>{{ $error }}</li>

                                        @endforeach

                                    </ul>

                                </div>

                            @endif

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.home_ar')}} </label>
                                <input type="text" class="form-control"  name="home_ar" value="{{$home->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.home_en')}} </label>
                                <input type="text" class="form-control"  name="home_en" value="{{$home->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.services_ar')}} </label>
                                <input type="text" class="form-control"  name="services_ar" value="{{$services->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.services_en')}} </label>
                                <input type="text" class="form-control"  name="services_en" value="{{$services->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.gallery_ar')}} </label>
                                <input type="text" class="form-control"  name="gallery_ar" value="{{$gallery->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.gallery_en')}} </label>
                                <input type="text" class="form-control"  name="gallery_en" value="{{$gallery->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.features_ar')}} </label>
                                <input type="text" class="form-control"  name="feature_ar" value="{{$feature->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.features_en')}} </label>
                                <input type="text" class="form-control"  name="feature_en" value="{{$feature->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.award_ar')}} </label>
                                <input type="text" class="form-control"  name="award_ar" value="{{$award->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.award_en')}} </label>
                                <input type="text" class="form-control"  name="award_en" value="{{$award->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.pricing_ar')}} </label>
                                <input type="text" class="form-control"  name="pricing_ar" value="{{$pricing->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.pricing_en')}} </label>
                                <input type="text" class="form-control"  name="pricing_en" value="{{$pricing->translate('en')->title}}">
                            </div>

                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.faq_ar')}} </label>
                                <input type="text" class="form-control"  name="faq_ar" value="{{$faq->translate('ar')->title}}">
                            </div>
                            <div class="col-md-6">
                                <label for="inputEmail4" class="form-label">{{trans('admin.faq_en')}} </label>
                                <input type="text" class="form-control"  name="faq_en" value="{{$faq->translate('en')->title}}">
                            </div>

                            <div class="col-12">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">{{trans('admin.edit')}}</button>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
