@php
    $lang = App::getLocale()
@endphp
@extends('admin.dashboard')

@section('content')
    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active"> {{trans('sidebar.prices')}} </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->


            <!-- Content -->
            <div class="col-12">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                         @if(session()->has('message'))
                        <div class="alert alert-success">
                            @if(session()->get('message')=='added_successfully')
                                {{trans('admin.added_successfully')}}
                            @elseif(session()->get('message')=='edited_successfully')
                                {{trans('admin.edited_successfully')}}
                            @elseif(session()->get('message')=='deleted_successfully')
                                {{trans('admin.deleted_successfully')}}
                            @endif
                        </div>
                    @endif

                        <div class="table-responsive" id="t1">
                                    <a class="btn btn-primary mb-2 me-4" href="{{route('admin_panel.prices.create')}}">{{trans('admin.add_price')}}  </a>
                                    <table id="myTable1" class="table table-striped table-bordered table-sm">
                                        <thead>
                                        <tr>
                                            <th class="text-center" scope="col"></th>
                                            <th scope="col">{{trans('admin.duration_ar')}} </th>
                                            <th scope="col">{{trans('admin.duration_en')}} </th>
                                            <th scope="col">{{trans('admin.title_ar')}} </th>
                                            <th scope="col">{{trans('admin.title_en')}} </th>
                                            <th scope="col">{{trans('admin.price')}}</th>
                                            <th class="text-center" scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php $counter=1;?>
                                        @foreach($prices as $price)

                                            <tr>

                                                <td>
                                                    <p class="text-center">{{$counter}}</p>
                                                    <span class="text-success"></span>
                                                    <?php $counter++;?>
                                                </td>

                                                <td>
                                                    <p class="mb-0">{{$price->translate('ar')->duration}}</p>
                                                    <span class="text-success"></span>
                                                </td>
                                                <td>
                                                    <p class="mb-0">{{$price->translate('en')->duration}}</p>
                                                    <span class="text-success"></span>
                                                </td>
                                                <td>
                                                    <p class="mb-0">{{$price->translate('ar')->title}}</p>
                                                    <span class="text-success"></span>
                                                </td>
                                                <td>
                                                    <p class="mb-0">{{$price->translate('en')->title}}</p>
                                                    <span class="text-success"></span>
                                                </td>
                                                <td>
                                                    <p class="mb-0">{{$price->price}}</p>
                                                    <span class="text-success"></span>
                                                </td>

                                                <td class="text-center">
                                                    <div class="action-btns">
                                                        <a href="{{route('admin_panel.prices.show',$price->id)}}" class="action-btn btn-view bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="{{trans('admin.show')}}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                        </a>
                                                        <a href="{{route('admin_panel.prices.edit',$price->id)}}" class="action-btn btn-edit bs-tooltip me-2" data-toggle="tooltip" data-placement="top" title="{{trans('admin.edit')}}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg>
                                                        </a>
                                                        <a href="/admin_panel/del_price/{{$price->id}}" class="action-btn btn-delete bs-tooltip" data-toggle="tooltip" data-placement="top" title="{{trans('admin.del')}}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                        </a>

                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                             <div class="col-sm-12 col-md-7">
                                    {{ $prices->links() }}
                            </div>

                                </div>
                    </div>
                </div>
        </div>

        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

@endsection
