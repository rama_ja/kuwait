@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active">{{trans('sidebar.prices_text')}} </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->
        <div class="layout-spacing">

            <!-- Content -->
            <div class="col-12">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                        <h3 class="mt-4"> </h3>
                        <form class="row g-3" method="post" action="{{route('admin_panel.price_text.update',1)}}" enctype="multipart/form-data" >
                                    @method('PATCH')
                                    @csrf
                                    @if ($errors->any())

                                        <div class="alert alert-danger">

                                            <ul style="list-style: none;margin:0">

                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach

                                            </ul>

                                        </div>

                                    @endif



                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.title_ar')}} </label>
                                        <input type="text" class="form-control"  name="title_ar" value="{{$price->translate('ar')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.title_en')}}</label>
                                        <input type="text" class="form-control"  name="title_en" value="{{$price->translate('en')->title}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.des_ar')}} </label>
                                        <textarea  class="form-control"  name="description_ar" >{{$price->translate('ar')->description}}</textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label"> {{trans('admin.des_en')}}</label>
                                        <textarea class="form-control"  name="description_en" >{{$price->translate('en')->description}}</textarea>
                                    </div>

                                    <div class="col-12">
                                        <div class="">
                                            <button type="submit" class="btn btn-primary"> {{trans('admin.edit')}}</button>
                                        </div>
                                    </div>


                                </form>
                    </div>
                </div>
            </div>
        </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->

    </div>



@endsection
