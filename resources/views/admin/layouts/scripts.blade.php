<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('js/mousetrap.min.js') }}"></script>
<script src="{{ asset('js/waves.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('js/apexcharts.min.js') }}"></script>
<script src="{{ asset('js/dash_1.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->


<!-- Icons -->
{{--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--}}
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="{{asset('icons/dist/js/bootstrap-iconpicker.bundle.min.js')}}"></script>

<!-- End Icons -->
