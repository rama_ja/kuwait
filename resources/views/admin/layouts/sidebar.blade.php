<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">

        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
{{--            @if($admin->type=='1')--}}
{{--                <li class="menu {{ ( (request()->is('*/users*'))|| (request()->is('*/admins*')) || (request()->is('*/create_user')) )? 'active' : '' }}">--}}
{{--                    <a href="#dashboard" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/users*')) || (request()->is('*/admins*')) || (request()->is('*/create_user')) ) ? 'true' : 'false' }}" class="dropdown-toggle">--}}
{{--                        <div class="">--}}
{{--                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>--}}
{{--                            <span>{{trans('sidebar.all_users')}}</span>--}}
{{--                        </div>--}}
{{--                        <div>--}}
{{--                            <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/users*'))  || (request()->is('*/admins*')) || (request()->is('*/create_user')) ) ? 'show' : ''}}" id="dashboard" data-bs-parent="#accordionExample">--}}
{{--                        <li class="{{ (request()->is('*/admins*')) ? 'active' : '' }}">--}}
{{--                            <a href="{{route('admin_panel.admins.index')}}"> {{trans('sidebar.admins')}} </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/users*')) || (request()->is('*/admins*')) || (request()->is('*/create_user')) ) ? 'show' : ''}}" id="dashboard" data-bs-parent="#accordionExample">--}}
{{--                        <li class="{{  ( (request()->is('*/users*')) || (request()->is('*/create_user')) ) ? 'active' : '' }}">--}}
{{--                            <a href="/admin_panel/users"> {{trans('sidebar.users')}} </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            @else--}}
{{--                <li class="menu {{ (request()->is('*/users*')) || (request()->is('*/users')) ? 'active' : '' }}">--}}
{{--                    <a href="/admin_panel/users" aria-expanded="false" class="dropdown-toggle">--}}
{{--                        <div class="">--}}
{{--                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>--}}
{{--                            <span> {{trans('sidebar.users')}} </span>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--            @if($admin->type=='1')--}}
{{--                <li class="menu {{ ( (request()->is('*/settings*'))|| (request()->is('*/metas*'))|| (request()->is('*/sections*')) )? 'active' : '' }}">--}}
{{--                    <a href="#settings" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/settings*')) || (request()->is('*/metas*'))|| (request()->is('*/sections*')) ) ? 'true' : 'false' }}" class="dropdown-toggle">--}}
{{--                        <div class="">--}}
{{--                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>--}}
{{--                            <span>{{trans('sidebar.general_settings')}} </span>--}}
{{--                        </div>--}}
{{--                        <div>--}}
{{--                            <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                    <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/settings*'))  || (request()->is('*/metas*')) || (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">--}}
{{--                        <li class="{{ (request()->is('*/settings*')) ? 'active' : '' }}">--}}
{{--                            <a href="{{route('admin_panel.settings.index')}}"> {{trans('sidebar.appearance')}} </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/settings*')) || (request()->is('*/metas*')) || (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">--}}
{{--                        <li class="{{  (request()->is('*/metas*')) ? 'active' : '' }}">--}}
{{--                            <a href="{{route('admin_panel.metas.index')}}"> {{trans('sidebar.settings')}}  </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                    <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/settings*')) || (request()->is('*/metas*'))|| (request()->is('*/sections*')) ) ? 'show' : ''}}" id="settings" data-bs-parent="#accordionExample">--}}
{{--                        <li class="{{  (request()->is('*/sections*')) ? 'active' : '' }}">--}}
{{--                            <a href="{{route('admin_panel.sections.index')}}"> {{trans('sidebar.sections')}}  </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--            @endif--}}

{{--            <li class="menu {{ ( (request()->is('*/headers*'))  || (request()->is('*/services*'))|| (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) )? 'active' : '' }}">--}}
{{--                <a href="#cont" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'true' : 'false' }}" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-archive"><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg>--}}
{{--                        <span>{{trans('sidebar.content')}} </span>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--                <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/headers*'))  ||(request()->is('*/services*'))  || (request()->is('*/gallery*')) || (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{ (request()->is('*/headers*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.headers.index')}}"> {{trans('sidebar.header')}} </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/headers*'))  || (request()->is('*/services*'))  || (request()->is('*/gallery*')) || (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{ (request()->is('*/services*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.services.index')}}"> {{trans('sidebar.services')}} </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*')) || (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/gallery*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.gallery.index')}}"> {{trans('sidebar.gallery')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ((request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*')) || (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/features*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.features.index')}}"> {{trans('sidebar.features')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*')) || (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/sliders*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.sliders.index')}}"> {{trans('sidebar.award')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ((request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*')) || (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/questions*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.questions.index')}}"> {{trans('sidebar.frequently_questions')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ((request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/prices*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.prices.index')}}"> {{trans('sidebar.prices')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ((request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*')) || (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/price_text*')) ? 'active' : '' }}">--}}
{{--                        <a href="{{route('admin_panel.price_text.index')}}"> {{trans('sidebar.prices_text')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                @if($admin->type=='1')--}}
{{--                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/headers*')) || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/edit_whatsapp')) ? 'active' : '' }}">--}}
{{--                        <a href="/admin_panel/edit_whatsapp"> WhatsApp  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ((request()->is('*/headers*'))  || (request()->is('*/services*')) || (request()->is('*/gallery*'))|| (request()->is('*/features*'))|| (request()->is('*/sliders*'))|| (request()->is('*/questions*'))|| (request()->is('*/prices*'))|| (request()->is('*/price_text*'))|| (request()->is('*/edit_whatsapp'))|| (request()->is('*/edit_call')) ) ? 'show' : ''}}" id="cont" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/edit_call')) ? 'active' : '' }}">--}}
{{--                        <a href="/admin_panel/edit_call"> Call flyer  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                @endif--}}
{{--            </li>--}}

{{--            <li class="menu {{ ( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail'))  )? 'active' : '' }}">--}}
{{--                <a href="#com" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail')) ) ? 'true' : 'false' }}" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone-call"><path d="M15.05 5A5 5 0 0 1 19 8.95M15.05 1A9 9 0 0 1 23 8.94m-1 7.98v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg>--}}
{{--                        <span>{{trans('sidebar.comunication')}} </span>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/edit_phone'))|| (request()->is('*/edit_mail')) ) ? 'show' : ''}}" id="com" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/edit_phone')) ? 'active' : '' }}">--}}
{{--                        <a href="/admin_panel/edit_phone"> Phone number </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}

{{--                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/edit_phone*'))|| (request()->is('*/edit_mail')) ) ? 'show' : ''}}" id="com" data-bs-parent="#accordionExample">--}}
{{--                    <li class="{{  (request()->is('*/edit_mail')) ? 'active' : '' }}">--}}
{{--                        <a href="/admin_panel/edit_mail"> {{trans('sidebar.mail')}}  </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}

{{--            </li>--}}



{{--            <li class="menu {{ (request()->is('*/social*')) ? 'active' : '' }}">--}}
{{--                <a href="{{route('admin_panel.social.index')}}" aria-expanded="false" class="dropdown-toggle">--}}
{{--                    <div class="">--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tv"><rect x="2" y="7" width="20" height="15" rx="2" ry="2"></rect><polyline points="17 2 12 7 7 2"></polyline></svg>--}}
{{--                        <span>  {{trans('sidebar.social_media')}}  </span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}


            <li class="menu {{ ( (request()->is('*/about*'))|| (request()->is('*/employees*')) )? 'active' : '' }}">
                <a href="#office" data-bs-toggle="collapse" aria-expanded="{{(  (request()->is('*/about*'))|| (request()->is('*/employees*'))  ) ? 'true' : 'false' }}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-server"><rect x="2" y="2" width="20" height="8" rx="2" ry="2"></rect><rect x="2" y="14" width="20" height="8" rx="2" ry="2"></rect><line x1="6" y1="6" x2="6.01" y2="6"></line><line x1="6" y1="18" x2="6.01" y2="18"></line></svg>
                        <span>عن المكتب </span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/about*'))  || (request()->is('*/employees*')) ) ? 'show' : ''}}" id="office" data-bs-parent="#accordionExample">
                    <li class="{{ (request()->is('*/about*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.about.index')}}"> نبذة تعريفية </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/about*')) || (request()->is('*/employees*'))  ) ? 'show' : ''}}" id="office" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/employees*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.employees.index')}}"> العاملون  </a>
                    </li>
                </ul>
            </li>

            <li class="menu {{ (request()->is('*/news*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.news.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span>  الأخبار و الفعاليات  </span>
                    </div>
                </a>
            </li>


            <li class="menu {{ ( (request()->is('*/missions*'))|| (request()->is('*/certifications*')) )? 'active' : '' }}">
                <a href="#list" data-bs-toggle="collapse" aria-expanded="{{( (request()->is('*/missions*'))|| (request()->is('*/certifications*')) ) ? 'true' : 'false' }}" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key"><path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path></svg>
                        <span>اللوائح و القرارات </span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                    </div>
                </a>
                <ul class="collapse submenu list-unstyled {{  ( (request()->is('*/missions*'))|| (request()->is('*/certifications*')) ) ? 'show' : ''}}" id="list" data-bs-parent="#accordionExample">
                    <li class="{{ (request()->is('*/missions*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.missions.index')}}"> لائحة البعثات </a>
                    </li>
                </ul>
                <ul class="collapse submenu list-unstyled {{ ( (request()->is('*/missions*'))|| (request()->is('*/certifications*'))  ) ? 'show' : ''}}" id="list" data-bs-parent="#accordionExample">
                    <li class="{{  (request()->is('*/certifications*')) ? 'active' : '' }}">
                        <a href="{{route('admin_panel.certifications.index')}}">  لائحة الشهادات  </a>
                    </li>
                </ul>

            </li>

            <li class="menu {{ (request()->is('*/questions*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.questions.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                        <span> الأسئلة الشائعة  </span>
                    </div>
                </a>
            </li>

             <li class="menu {{ (request()->is('*/short_news*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.short_news.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                        <span> الأخبار القصيرة  </span>
                    </div>
                </a>
            </li>

            <li class="menu {{ (request()->is('*/countries*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.countries.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-compass"><circle cx="12" cy="12" r="10"></circle><polygon points="16.24 7.76 14.12 14.12 7.76 16.24 9.88 9.88 16.24 7.76"></polygon></svg>
                        <span> الدول   </span>
                    </div>
                </a>
            </li>

            <li class="menu {{ (request()->is('*/specialties*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.specialties.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-folder-plus"><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path><line x1="12" y1="11" x2="12" y2="17"></line><line x1="9" y1="14" x2="15" y2="14"></line></svg>
                        <span> التخصصات الرئيسية </span>
                    </div>
                </a>
            </li>


            <li class="menu {{ (request()->is('*/sub_specialties*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.sub_specialties.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-folder-minus"><path d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z"></path><line x1="9" y1="14" x2="15" y2="14"></line></svg> <span> التخصصات الفرعية </span>
                    </div>

                </a>
            </li>

            <li class="menu {{ (request()->is('*/notes*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.notes.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                        <span> الملاحظات  </span>
                    </div>
                </a>
            </li>

            <li class="menu {{ (request()->is('*/universities*')) ? 'active' : '' }}">
                <a href="{{route('admin_panel.universities.index')}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-book"><path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path><path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path></svg>
                        <span> الجامعات  </span>
                    </div>
                </a>
            </li>


        </ul>

    </nav>

</div>
<!--  END SIDEBAR  -->
