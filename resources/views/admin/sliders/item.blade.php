@extends('admin.dashboard')

@section('content')

    <div id="content" class="main-content">
        <!--  BEGIN BREADCRUMBS  -->
        <div class="secondary-nav">
            <div class="breadcrumbs-container" data-page-heading="Analytics">
                <header class="header navbar navbar-expand-sm">
                    <a href="javascript:void(0);" class="btn-toggle sidebarCollapse" data-placement="bottom">
                        <svg xmlns="http://www.w3.org/2000/.svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
                    </a>
                    <div class="d-flex breadcrumb-content">
                        <div class="page-header">

                            <div class="page-title">
                            </div>

                            <nav class="breadcrumb-style-one" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">{{trans('sidebar.award')}} </li>
                                    <li class="breadcrumb-item active"> {{trans('admin.show_award')}} </li>
                                </ol>
                            </nav>

                        </div>
                    </div>
                </header>
            </div>
        </div>
        <br>
        <!--  END BREADCRUMBS  -->

        <div class="layout-spacing">

            <!-- Content -->
            <div class="col-12">
                <div class="user-profile ">
                    <div class="widget-content widget-content-area">
                            <h3 class="mt-4"> </h3>
                        <div class="" style="padding: 2%; font-weight: bold;">
                            <div class="">

                                <div class="col-md-6 ">
                                    <label for="inputAddress" class="form-label">{{trans('admin.image')}} </label>
                                    <img  src="/{{$slider->image}}"  width="340" height="175"/>
                                    <br><br>
                                </div>
                                <div class="col-md-6">
                                    <label> {{trans('admin.title_ar')}}  : </label>
                                    @if($slider->translate('ar')->title)
                                        <span>{{$slider->translate('ar')->title}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>{{trans('admin.des_ar')}}  : </label>
                                    @if($slider->translate('ar')->description)
                                        <span>{{$slider->translate('ar')->description}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>{{trans('admin.title_en')}}  : </label>
                                    @if($slider->translate('en')->title)
                                        <span>{{$slider->translate('en')->title}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>{{trans('admin.des_en')}}  : </label>
                                    @if($slider->translate('en')->description)
                                        <span>{{$slider->translate('en')->description}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label >{{trans('admin.button_ar')}} : </label>
                                    @if($slider->translate('ar')->button_name)
                                        <span>{{$slider->translate('ar')->button_name}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label >{{trans('admin.button_en')}} : </label>
                                    @if($slider->translate('en')->button_name)
                                        <span>{{$slider->translate('en')->button_name}}</span>
                                    @else
                                        <span>لايوجد</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                  
                                        <label >{{trans('admin.web')}} : </label>

                                        <a target="_blank" href="/{{$slider->button_link}}">{{$slider->button_link}}</a>
                                </div>

                            </div>

                        </div>

                        </div>
                    </div>
                </div>
            </div>


        <!--  BEGIN FOOTER  -->
    @include('admin.layouts.footer')
    <!--  END FOOTER  -->
    </div>



@endsection
